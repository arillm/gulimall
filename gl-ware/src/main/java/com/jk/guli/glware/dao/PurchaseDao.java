package com.jk.guli.glware.dao;

import com.jk.guli.apiglware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:52
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
