package com.jk.guli.glware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jk.common.utils.PageUtils;
import com.jk.guli.apiglware.entity.PurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:52
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

