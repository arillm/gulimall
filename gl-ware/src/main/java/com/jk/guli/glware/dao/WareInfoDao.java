package com.jk.guli.glware.dao;

import com.jk.guli.apiglware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:51
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
