package com.jk.guli.glware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jk.common.utils.PageUtils;
import com.jk.guli.apiglware.entity.WareSkuEntity;

import java.util.Map;

/**
 * 商品库存
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:50
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

