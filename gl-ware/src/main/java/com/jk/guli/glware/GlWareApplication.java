package com.jk.guli.glware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients({"com.jk.guli"})
@EnableDiscoveryClient
@SpringBootApplication
public class GlWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(GlWareApplication.class, args);
    }

}
