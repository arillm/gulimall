package com.jk.guli.glware.dao;

import com.jk.guli.apiglware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:50
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
