package com.jk.guli.glorder.dao;

import com.jk.guli.apiglorder.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
