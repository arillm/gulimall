package com.jk.guli.glorder.dao;

import com.jk.guli.apiglorder.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
