package com.jk.guli.glorder.dao;

import com.jk.guli.apiglorder.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
