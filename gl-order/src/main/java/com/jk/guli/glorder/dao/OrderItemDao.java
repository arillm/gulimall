package com.jk.guli.glorder.dao;

import com.jk.guli.apiglorder.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:07
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
