package com.jk.guli.glorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients({"com.jk.guli"})
@EnableDiscoveryClient
@SpringBootApplication
public class GlOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GlOrderApplication.class, args);
    }

}
