package com.jk.guli.glorder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jk.common.utils.PageUtils;
import com.jk.guli.apiglorder.entity.OrderItemEntity;

import java.util.Map;

/**
 * 订单项信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:07
 */
public interface OrderItemService extends IService<OrderItemEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

