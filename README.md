# gulimall

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明

    - apiparents            api父文件夹：entity+api请求接口
        - api_coupon        优惠信息模块
        - api_member        用户信息模块
        - api_order         订单信息模块
        - api_product       产品信息模块
        - api_ware          信息模块
    - gl-coupon             优惠信息业务逻辑
    - gl-member             用户信息业务逻辑
    - gl-order              订单信息业务逻辑
    - gl-product            产品信息业务逻辑
    - gl-ware               业务逻辑
    - gl_common             工具项目
    - renren-fast           vue后台管理项目
    - renren-generator      人人开源代码生成项目
    - gl_gateway            gateway 网关路由项目

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


#### 码云特技




## feign的使用

1.  @EnableFeignClients({"com.jk.guli"})
2.  编写FeignClient接口 

        @FeignClient("glcoupon")
        public interface  CouponSpuRelationApiFeign {
        
        
            /**
             * 列表
             */
            @RequestMapping("/api/glcoupon/couponspurelation/list")
            public R list(@RequestParam Map<String, Object> params);
        }
3.  注入，调用

        @Autowired
        CouponApiFeign couponApiFeign;

## nacos的使用