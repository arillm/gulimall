package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.SkuFullReductionEntity;
import com.jk.common.utils.R;



/**
 * 商品满减信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:32
 */
@FeignClient("glcoupon")
public interface  SkuFullReductionApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/skufullreduction/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/skufullreduction/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/skufullreduction/save")
    public R save(@RequestBody SkuFullReductionEntity skuFullReduction);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/skufullreduction/update")
    public R update(@RequestBody SkuFullReductionEntity skuFullReduction);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/skufullreduction/delete")
    public R delete(@RequestBody Long[] ids);

}