package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.SeckillSkuRelationEntity;
import com.jk.common.utils.R;



/**
 * 秒杀活动商品关联
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  SeckillSkuRelationApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/seckillskurelation/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/seckillskurelation/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/seckillskurelation/save")
    public R save(@RequestBody SeckillSkuRelationEntity seckillSkuRelation);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/seckillskurelation/update")
    public R update(@RequestBody SeckillSkuRelationEntity seckillSkuRelation);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/seckillskurelation/delete")
    public R delete(@RequestBody Long[] ids);

}