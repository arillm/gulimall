package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.HomeAdvEntity;
import com.jk.common.utils.R;



/**
 * 首页轮播广告
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  HomeAdvApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/homeadv/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/homeadv/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/homeadv/save")
    public R save(@RequestBody HomeAdvEntity homeAdv);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/homeadv/update")
    public R update(@RequestBody HomeAdvEntity homeAdv);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/homeadv/delete")
    public R delete(@RequestBody Long[] ids);

}