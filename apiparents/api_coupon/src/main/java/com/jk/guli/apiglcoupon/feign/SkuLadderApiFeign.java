package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.SkuLadderEntity;
import com.jk.common.utils.R;



/**
 * 商品阶梯价格
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  SkuLadderApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/skuladder/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/skuladder/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/skuladder/save")
    public R save(@RequestBody SkuLadderEntity skuLadder);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/skuladder/update")
    public R update(@RequestBody SkuLadderEntity skuLadder);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/skuladder/delete")
    public R delete(@RequestBody Long[] ids);

}