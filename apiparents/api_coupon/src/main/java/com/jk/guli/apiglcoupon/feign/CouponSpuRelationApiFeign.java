package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.CouponSpuRelationEntity;
import com.jk.common.utils.R;



/**
 * 优惠券与产品关联
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  CouponSpuRelationApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/couponspurelation/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/couponspurelation/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/couponspurelation/save")
    public R save(@RequestBody CouponSpuRelationEntity couponSpuRelation);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/couponspurelation/update")
    public R update(@RequestBody CouponSpuRelationEntity couponSpuRelation);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/couponspurelation/delete")
    public R delete(@RequestBody Long[] ids);

}