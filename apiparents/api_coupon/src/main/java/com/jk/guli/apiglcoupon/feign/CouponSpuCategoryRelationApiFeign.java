package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.CouponSpuCategoryRelationEntity;
import com.jk.common.utils.R;



/**
 * 优惠券分类关联
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  CouponSpuCategoryRelationApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/couponspucategoryrelation/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/couponspucategoryrelation/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/couponspucategoryrelation/save")
    public R save(@RequestBody CouponSpuCategoryRelationEntity couponSpuCategoryRelation);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/couponspucategoryrelation/update")
    public R update(@RequestBody CouponSpuCategoryRelationEntity couponSpuCategoryRelation);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/couponspucategoryrelation/delete")
    public R delete(@RequestBody Long[] ids);

}