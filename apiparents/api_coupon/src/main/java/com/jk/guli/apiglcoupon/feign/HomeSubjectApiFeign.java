package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.HomeSubjectEntity;
import com.jk.common.utils.R;



/**
 * 首页专题表【jd首页下面很多专题，每个专题链接新的页面，展示专题商品信息】
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  HomeSubjectApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/homesubject/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/homesubject/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/homesubject/save")
    public R save(@RequestBody HomeSubjectEntity homeSubject);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/homesubject/update")
    public R update(@RequestBody HomeSubjectEntity homeSubject);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/homesubject/delete")
    public R delete(@RequestBody Long[] ids);

}