package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.SeckillPromotionEntity;
import com.jk.common.utils.R;



/**
 * 秒杀活动
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  SeckillPromotionApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/seckillpromotion/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/seckillpromotion/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/seckillpromotion/save")
    public R save(@RequestBody SeckillPromotionEntity seckillPromotion);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/seckillpromotion/update")
    public R update(@RequestBody SeckillPromotionEntity seckillPromotion);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/seckillpromotion/delete")
    public R delete(@RequestBody Long[] ids);

}