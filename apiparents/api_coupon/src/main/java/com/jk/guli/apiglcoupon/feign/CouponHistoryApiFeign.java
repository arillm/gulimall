package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.CouponHistoryEntity;
import com.jk.common.utils.R;



/**
 * 优惠券领取历史记录
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  CouponHistoryApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/couponhistory/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/couponhistory/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/couponhistory/save")
    public R save(@RequestBody CouponHistoryEntity couponHistory);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/couponhistory/update")
    public R update(@RequestBody CouponHistoryEntity couponHistory);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/couponhistory/delete")
    public R delete(@RequestBody Long[] ids);

}