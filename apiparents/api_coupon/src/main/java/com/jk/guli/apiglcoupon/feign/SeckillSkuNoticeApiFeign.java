package com.jk.guli.apiglcoupon.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglcoupon.entity.SeckillSkuNoticeEntity;
import com.jk.common.utils.R;



/**
 * 秒杀商品通知订阅
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@FeignClient("glcoupon")
public interface  SeckillSkuNoticeApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glcoupon/seckillskunotice/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glcoupon/seckillskunotice/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glcoupon/seckillskunotice/save")
    public R save(@RequestBody SeckillSkuNoticeEntity seckillSkuNotice);

    /**
     * 修改
     */
    @RequestMapping("/api/glcoupon/seckillskunotice/update")
    public R update(@RequestBody SeckillSkuNoticeEntity seckillSkuNotice);

    /**
     * 删除
     */
    @RequestMapping("/api/glcoupon/seckillskunotice/delete")
    public R delete(@RequestBody Long[] ids);

}