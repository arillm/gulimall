package com.jk.guli.apiglware.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglware.entity.PurchaseEntity;
import com.jk.common.utils.R;



/**
 * 采购信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:52
 */
@FeignClient("glware")
public interface  PurchaseApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glware/purchase/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glware/purchase/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glware/purchase/save")
    public R save(@RequestBody PurchaseEntity purchase);

    /**
     * 修改
     */
    @RequestMapping("/api/glware/purchase/update")
    public R update(@RequestBody PurchaseEntity purchase);

    /**
     * 删除
     */
    @RequestMapping("/api/glware/purchase/delete")
    public R delete(@RequestBody Long[] ids);

}