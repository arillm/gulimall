package com.jk.guli.apiglware.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglware.entity.WareSkuEntity;
import com.jk.common.utils.R;



/**
 * 商品库存
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:50
 */
@FeignClient("glware")
public interface  WareSkuApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glware/waresku/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glware/waresku/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glware/waresku/save")
    public R save(@RequestBody WareSkuEntity wareSku);

    /**
     * 修改
     */
    @RequestMapping("/api/glware/waresku/update")
    public R update(@RequestBody WareSkuEntity wareSku);

    /**
     * 删除
     */
    @RequestMapping("/api/glware/waresku/delete")
    public R delete(@RequestBody Long[] ids);

}