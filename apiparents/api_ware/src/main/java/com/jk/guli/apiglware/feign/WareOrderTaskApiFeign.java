package com.jk.guli.apiglware.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglware.entity.WareOrderTaskEntity;
import com.jk.common.utils.R;



/**
 * 库存工作单
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:51
 */
@FeignClient("glware")
public interface  WareOrderTaskApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glware/wareordertask/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glware/wareordertask/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glware/wareordertask/save")
    public R save(@RequestBody WareOrderTaskEntity wareOrderTask);

    /**
     * 修改
     */
    @RequestMapping("/api/glware/wareordertask/update")
    public R update(@RequestBody WareOrderTaskEntity wareOrderTask);

    /**
     * 删除
     */
    @RequestMapping("/api/glware/wareordertask/delete")
    public R delete(@RequestBody Long[] ids);

}