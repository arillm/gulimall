package com.jk.guli.apiglware.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglware.entity.PurchaseDetailEntity;
import com.jk.common.utils.R;



/**
 * 
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 14:55:52
 */
@FeignClient("glware")
public interface  PurchaseDetailApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glware/purchasedetail/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glware/purchasedetail/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glware/purchasedetail/save")
    public R save(@RequestBody PurchaseDetailEntity purchaseDetail);

    /**
     * 修改
     */
    @RequestMapping("/api/glware/purchasedetail/update")
    public R update(@RequestBody PurchaseDetailEntity purchaseDetail);

    /**
     * 删除
     */
    @RequestMapping("/api/glware/purchasedetail/delete")
    public R delete(@RequestBody Long[] ids);

}