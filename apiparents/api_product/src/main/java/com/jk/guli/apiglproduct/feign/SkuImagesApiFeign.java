package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.SkuImagesEntity;
import com.jk.common.utils.R;



/**
 * sku图片
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  SkuImagesApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/skuimages/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/skuimages/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/skuimages/save")
    public R save(@RequestBody SkuImagesEntity skuImages);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/skuimages/update")
    public R update(@RequestBody SkuImagesEntity skuImages);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/skuimages/delete")
    public R delete(@RequestBody Long[] ids);

}