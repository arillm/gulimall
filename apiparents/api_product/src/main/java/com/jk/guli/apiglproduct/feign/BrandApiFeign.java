package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.BrandEntity;
import com.jk.common.utils.R;



/**
 * 品牌
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  BrandApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/brand/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/brand/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/brand/save")
    public R save(@RequestBody BrandEntity brand);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/brand/update")
    public R update(@RequestBody BrandEntity brand);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/brand/delete")
    public R delete(@RequestBody Long[] brandIds);

}