package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.ProductAttrValueEntity;
import com.jk.common.utils.R;



/**
 * spu属性值
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  ProductAttrValueApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/productattrvalue/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/productattrvalue/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/productattrvalue/save")
    public R save(@RequestBody ProductAttrValueEntity productAttrValue);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/productattrvalue/update")
    public R update(@RequestBody ProductAttrValueEntity productAttrValue);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/productattrvalue/delete")
    public R delete(@RequestBody Long[] ids);

}