package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.AttrGroupEntity;
import com.jk.common.utils.R;



/**
 * 属性分组
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  AttrGroupApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/attrgroup/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/attrgroup/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/attrgroup/save")
    public R save(@RequestBody AttrGroupEntity attrGroup);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/attrgroup/update")
    public R update(@RequestBody AttrGroupEntity attrGroup);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/attrgroup/delete")
    public R delete(@RequestBody Long[] attrGroupIds);

}