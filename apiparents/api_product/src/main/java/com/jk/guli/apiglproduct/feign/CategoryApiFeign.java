package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.CategoryEntity;
import com.jk.common.utils.R;



/**
 * 商品三级分类
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  CategoryApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/category/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/category/info/{catId}")
    public R info(@PathVariable("catId") Long catId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/category/save")
    public R save(@RequestBody CategoryEntity category);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/category/update")
    public R update(@RequestBody CategoryEntity category);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/category/delete")
    public R delete(@RequestBody Long[] catIds);

}