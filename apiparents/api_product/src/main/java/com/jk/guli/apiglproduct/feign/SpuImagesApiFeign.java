package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.SpuImagesEntity;
import com.jk.common.utils.R;



/**
 * spu图片
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  SpuImagesApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/spuimages/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/spuimages/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/spuimages/save")
    public R save(@RequestBody SpuImagesEntity spuImages);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/spuimages/update")
    public R update(@RequestBody SpuImagesEntity spuImages);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/spuimages/delete")
    public R delete(@RequestBody Long[] ids);

}