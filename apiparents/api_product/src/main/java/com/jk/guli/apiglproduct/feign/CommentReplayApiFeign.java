package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.CommentReplayEntity;
import com.jk.common.utils.R;



/**
 * 商品评价回复关系
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  CommentReplayApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/commentreplay/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/commentreplay/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/commentreplay/save")
    public R save(@RequestBody CommentReplayEntity commentReplay);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/commentreplay/update")
    public R update(@RequestBody CommentReplayEntity commentReplay);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/commentreplay/delete")
    public R delete(@RequestBody Long[] ids);

}