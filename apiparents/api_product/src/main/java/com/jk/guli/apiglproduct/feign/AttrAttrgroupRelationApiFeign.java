package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.AttrAttrgroupRelationEntity;
import com.jk.common.utils.R;



/**
 * 属性&属性分组关联
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  AttrAttrgroupRelationApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/attrattrgrouprelation/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/attrattrgrouprelation/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/attrattrgrouprelation/save")
    public R save(@RequestBody AttrAttrgroupRelationEntity attrAttrgroupRelation);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/attrattrgrouprelation/update")
    public R update(@RequestBody AttrAttrgroupRelationEntity attrAttrgroupRelation);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/attrattrgrouprelation/delete")
    public R delete(@RequestBody Long[] ids);

}