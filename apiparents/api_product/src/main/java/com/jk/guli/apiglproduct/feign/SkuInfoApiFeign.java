package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.SkuInfoEntity;
import com.jk.common.utils.R;



/**
 * sku信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  SkuInfoApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/skuinfo/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/skuinfo/save")
    public R save(@RequestBody SkuInfoEntity skuInfo);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/skuinfo/update")
    public R update(@RequestBody SkuInfoEntity skuInfo);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/skuinfo/delete")
    public R delete(@RequestBody Long[] skuIds);

}