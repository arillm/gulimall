package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.AttrEntity;
import com.jk.common.utils.R;



/**
 * 商品属性
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  AttrApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/attr/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/attr/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/attr/save")
    public R save(@RequestBody AttrEntity attr);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/attr/update")
    public R update(@RequestBody AttrEntity attr);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/attr/delete")
    public R delete(@RequestBody Long[] attrIds);

}