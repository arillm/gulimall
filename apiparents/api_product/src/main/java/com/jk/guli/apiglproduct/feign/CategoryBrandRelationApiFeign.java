package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.CategoryBrandRelationEntity;
import com.jk.common.utils.R;



/**
 * 品牌分类关联
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  CategoryBrandRelationApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/categorybrandrelation/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/categorybrandrelation/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/categorybrandrelation/save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/categorybrandrelation/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/categorybrandrelation/delete")
    public R delete(@RequestBody Long[] ids);

}