package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.SpuInfoDescEntity;
import com.jk.common.utils.R;



/**
 * spu信息介绍
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  SpuInfoDescApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/spuinfodesc/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/spuinfodesc/info/{spuId}")
    public R info(@PathVariable("spuId") Long spuId);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/spuinfodesc/save")
    public R save(@RequestBody SpuInfoDescEntity spuInfoDesc);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/spuinfodesc/update")
    public R update(@RequestBody SpuInfoDescEntity spuInfoDesc);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/spuinfodesc/delete")
    public R delete(@RequestBody Long[] spuIds);

}