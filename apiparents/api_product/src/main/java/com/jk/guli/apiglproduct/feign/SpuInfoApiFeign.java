package com.jk.guli.apiglproduct.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglproduct.entity.SpuInfoEntity;
import com.jk.common.utils.R;



/**
 * spu信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@FeignClient("glproduct")
public interface  SpuInfoApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glproduct/spuinfo/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glproduct/spuinfo/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glproduct/spuinfo/save")
    public R save(@RequestBody SpuInfoEntity spuInfo);

    /**
     * 修改
     */
    @RequestMapping("/api/glproduct/spuinfo/update")
    public R update(@RequestBody SpuInfoEntity spuInfo);

    /**
     * 删除
     */
    @RequestMapping("/api/glproduct/spuinfo/delete")
    public R delete(@RequestBody Long[] ids);

}