package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.IntegrationChangeHistoryEntity;
import com.jk.common.utils.R;



/**
 * 积分变化历史记录
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@FeignClient("glmember")
public interface  IntegrationChangeHistoryApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/integrationchangehistory/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/integrationchangehistory/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/integrationchangehistory/save")
    public R save(@RequestBody IntegrationChangeHistoryEntity integrationChangeHistory);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/integrationchangehistory/update")
    public R update(@RequestBody IntegrationChangeHistoryEntity integrationChangeHistory);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/integrationchangehistory/delete")
    public R delete(@RequestBody Long[] ids);

}