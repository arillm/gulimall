package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.GrowthChangeHistoryEntity;
import com.jk.common.utils.R;



/**
 * 成长值变化历史记录
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:46
 */
@FeignClient("glmember")
public interface  GrowthChangeHistoryApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/growthchangehistory/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/growthchangehistory/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/growthchangehistory/save")
    public R save(@RequestBody GrowthChangeHistoryEntity growthChangeHistory);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/growthchangehistory/update")
    public R update(@RequestBody GrowthChangeHistoryEntity growthChangeHistory);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/growthchangehistory/delete")
    public R delete(@RequestBody Long[] ids);

}