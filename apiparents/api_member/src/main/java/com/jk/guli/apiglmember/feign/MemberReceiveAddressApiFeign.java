package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.MemberReceiveAddressEntity;
import com.jk.common.utils.R;



/**
 * 会员收货地址
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@FeignClient("glmember")
public interface  MemberReceiveAddressApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/memberreceiveaddress/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/memberreceiveaddress/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/memberreceiveaddress/save")
    public R save(@RequestBody MemberReceiveAddressEntity memberReceiveAddress);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/memberreceiveaddress/update")
    public R update(@RequestBody MemberReceiveAddressEntity memberReceiveAddress);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/memberreceiveaddress/delete")
    public R delete(@RequestBody Long[] ids);

}