package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.MemberCollectSubjectEntity;
import com.jk.common.utils.R;



/**
 * 会员收藏的专题活动
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@FeignClient("glmember")
public interface  MemberCollectSubjectApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/membercollectsubject/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/membercollectsubject/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/membercollectsubject/save")
    public R save(@RequestBody MemberCollectSubjectEntity memberCollectSubject);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/membercollectsubject/update")
    public R update(@RequestBody MemberCollectSubjectEntity memberCollectSubject);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/membercollectsubject/delete")
    public R delete(@RequestBody Long[] ids);

}