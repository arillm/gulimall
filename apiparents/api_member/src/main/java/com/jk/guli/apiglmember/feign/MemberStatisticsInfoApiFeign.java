package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.MemberStatisticsInfoEntity;
import com.jk.common.utils.R;



/**
 * 会员统计信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@FeignClient("glmember")
public interface  MemberStatisticsInfoApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/memberstatisticsinfo/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/memberstatisticsinfo/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/memberstatisticsinfo/save")
    public R save(@RequestBody MemberStatisticsInfoEntity memberStatisticsInfo);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/memberstatisticsinfo/update")
    public R update(@RequestBody MemberStatisticsInfoEntity memberStatisticsInfo);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/memberstatisticsinfo/delete")
    public R delete(@RequestBody Long[] ids);

}