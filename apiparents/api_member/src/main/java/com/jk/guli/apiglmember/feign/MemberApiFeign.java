package com.jk.guli.apiglmember.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglmember.entity.MemberEntity;
import com.jk.common.utils.R;



/**
 * 会员
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@FeignClient("glmember")
public interface  MemberApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glmember/member/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glmember/member/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glmember/member/save")
    public R save(@RequestBody MemberEntity member);

    /**
     * 修改
     */
    @RequestMapping("/api/glmember/member/update")
    public R update(@RequestBody MemberEntity member);

    /**
     * 删除
     */
    @RequestMapping("/api/glmember/member/delete")
    public R delete(@RequestBody Long[] ids);

}