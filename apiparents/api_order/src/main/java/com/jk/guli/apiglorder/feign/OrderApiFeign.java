package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderEntity;
import com.jk.common.utils.R;



/**
 * 订单
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:07
 */
@FeignClient("glorder")
public interface  OrderApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/order/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/order/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/order/save")
    public R save(@RequestBody OrderEntity order);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/order/update")
    public R update(@RequestBody OrderEntity order);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/order/delete")
    public R delete(@RequestBody Long[] ids);

}