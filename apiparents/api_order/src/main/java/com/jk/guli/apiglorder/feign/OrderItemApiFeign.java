package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderItemEntity;
import com.jk.common.utils.R;



/**
 * 订单项信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:07
 */
@FeignClient("glorder")
public interface  OrderItemApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/orderitem/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/orderitem/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/orderitem/save")
    public R save(@RequestBody OrderItemEntity orderItem);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/orderitem/update")
    public R update(@RequestBody OrderItemEntity orderItem);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/orderitem/delete")
    public R delete(@RequestBody Long[] ids);

}