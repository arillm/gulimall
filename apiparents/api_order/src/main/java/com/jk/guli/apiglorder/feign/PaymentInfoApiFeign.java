package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.PaymentInfoEntity;
import com.jk.common.utils.R;



/**
 * 支付信息表
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@FeignClient("glorder")
public interface  PaymentInfoApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/paymentinfo/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/paymentinfo/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/paymentinfo/save")
    public R save(@RequestBody PaymentInfoEntity paymentInfo);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/paymentinfo/update")
    public R update(@RequestBody PaymentInfoEntity paymentInfo);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/paymentinfo/delete")
    public R delete(@RequestBody Long[] ids);

}