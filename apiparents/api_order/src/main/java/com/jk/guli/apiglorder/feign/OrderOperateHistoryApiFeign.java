package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderOperateHistoryEntity;
import com.jk.common.utils.R;



/**
 * 订单操作历史记录
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:07
 */
@FeignClient("glorder")
public interface  OrderOperateHistoryApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/orderoperatehistory/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/orderoperatehistory/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/orderoperatehistory/save")
    public R save(@RequestBody OrderOperateHistoryEntity orderOperateHistory);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/orderoperatehistory/update")
    public R update(@RequestBody OrderOperateHistoryEntity orderOperateHistory);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/orderoperatehistory/delete")
    public R delete(@RequestBody Long[] ids);

}