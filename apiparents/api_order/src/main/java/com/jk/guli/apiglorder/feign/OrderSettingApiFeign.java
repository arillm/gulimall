package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderSettingEntity;
import com.jk.common.utils.R;



/**
 * 订单配置信息
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@FeignClient("glorder")
public interface  OrderSettingApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/ordersetting/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/ordersetting/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/ordersetting/save")
    public R save(@RequestBody OrderSettingEntity orderSetting);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/ordersetting/update")
    public R update(@RequestBody OrderSettingEntity orderSetting);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/ordersetting/delete")
    public R delete(@RequestBody Long[] ids);

}