package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderReturnReasonEntity;
import com.jk.common.utils.R;



/**
 * 退货原因
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@FeignClient("glorder")
public interface  OrderReturnReasonApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/orderreturnreason/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/orderreturnreason/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/orderreturnreason/save")
    public R save(@RequestBody OrderReturnReasonEntity orderReturnReason);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/orderreturnreason/update")
    public R update(@RequestBody OrderReturnReasonEntity orderReturnReason);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/orderreturnreason/delete")
    public R delete(@RequestBody Long[] ids);

}