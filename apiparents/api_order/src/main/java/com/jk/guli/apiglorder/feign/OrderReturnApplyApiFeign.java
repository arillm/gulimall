package com.jk.guli.apiglorder.feign;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.cloud.openfeign.FeignClient;

import com.jk.guli.apiglorder.entity.OrderReturnApplyEntity;
import com.jk.common.utils.R;



/**
 * 订单退货申请
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:06
 */
@FeignClient("glorder")
public interface  OrderReturnApplyApiFeign {


    /**
     * 列表
     */
    @RequestMapping("/api/glorder/orderreturnapply/list")
    public R list(@RequestParam Map<String, Object> params);


    /**
     * 信息
     */
    @RequestMapping("/api/glorder/orderreturnapply/info/{id}")
    public R info(@PathVariable("id") Long id);

    /**
     * 保存
     */
    @RequestMapping("/api/glorder/orderreturnapply/save")
    public R save(@RequestBody OrderReturnApplyEntity orderReturnApply);

    /**
     * 修改
     */
    @RequestMapping("/api/glorder/orderreturnapply/update")
    public R update(@RequestBody OrderReturnApplyEntity orderReturnApply);

    /**
     * 删除
     */
    @RequestMapping("/api/glorder/orderreturnapply/delete")
    public R delete(@RequestBody Long[] ids);

}