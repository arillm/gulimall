package com.jk.guli.glmember.controller;

import java.util.Arrays;
import java.util.Map;


import com.jk.guli.apiglcoupon.entity.CouponEntity;
import com.jk.guli.apiglcoupon.feign.CouponApiFeign;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jk.guli.apiglmember.entity.MemberEntity;
import com.jk.guli.glmember.service.MemberService;
import com.jk.common.utils.PageUtils;
import com.jk.common.utils.R;



/**
 * 会员
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@RequestMapping("/api/glmember/member")
@RestController
public class MemberController{
    @Autowired
    private MemberService memberService;

    @Autowired
    CouponApiFeign couponApiFeign;



    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("glmember:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);


        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCode("cefsdafasdfasdf");
        couponApiFeign.save(couponEntity);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("glmember:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("glmember:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("glmember:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("glmember:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
