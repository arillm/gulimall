package com.jk.guli.glmember.dao;

import com.jk.guli.apiglmember.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
