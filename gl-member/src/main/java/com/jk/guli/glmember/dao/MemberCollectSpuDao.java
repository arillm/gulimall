package com.jk.guli.glmember.dao;

import com.jk.guli.apiglmember.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
