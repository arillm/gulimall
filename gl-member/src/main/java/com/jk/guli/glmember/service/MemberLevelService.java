package com.jk.guli.glmember.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jk.common.utils.PageUtils;
import com.jk.guli.apiglmember.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:02:45
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

