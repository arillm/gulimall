package com.jk.guli.glproduct.dao;

import com.jk.guli.apiglproduct.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:01:18
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
