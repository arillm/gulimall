package com.jk.guli.glproduct;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.jk.guli.apiglproduct.entity.BrandEntity;
import com.jk.guli.glproduct.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GlProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setDescript("this is test");
        brandService.save(brandEntity);
    }

}
