package com.jk.guli.glcoupon;


import com.jk.guli.apiglcoupon.entity.CouponEntity;
import com.jk.guli.glcoupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class GlCouponApplicationTests {

    @Autowired
    CouponService couponService;

    @Test
    void contextLoads() {

        CouponEntity smsCouponEntity = new CouponEntity();
        smsCouponEntity.setCode("dasdfasdfad");
        couponService.save(smsCouponEntity);

    }

}
