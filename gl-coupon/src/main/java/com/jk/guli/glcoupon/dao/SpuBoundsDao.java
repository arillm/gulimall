package com.jk.guli.glcoupon.dao;

import com.jk.guli.apiglcoupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:32
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
