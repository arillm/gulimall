package com.jk.guli.glcoupon.dao;

import com.jk.guli.apiglcoupon.entity.CouponSpuCategoryRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券分类关联
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@Mapper
public interface CouponSpuCategoryRelationDao extends BaseMapper<CouponSpuCategoryRelationEntity> {
	
}
