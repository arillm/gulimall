package com.jk.guli.glcoupon.dao;

import com.jk.guli.apiglcoupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author jk
 * @email 514181780@qq.com
 * @date 2020-06-24 15:03:33
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
